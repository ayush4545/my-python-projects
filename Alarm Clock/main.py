from time import *
import datetime
from tkinter import *
from ttkthemes import themed_tk as tk
from tkinter import ttk
import tkinter.messagebox as tmsg
from playsound import playsound
from win10toast import ToastNotifier


def alarm():
    h = hour.get()
    m = minute.get()
    s = second.get()
    set_alarm = h + ":" + m + ":" + s
    if (h == "") and (m == "") and (s == ""):
        tmsg.showerror('error', 'Please fill atleast one field', icon='warning')
    else:
        toast = ToastNotifier()
        while True:
            sleep(1)
            date = datetime.datetime.now()
            now = date.strftime("%H:%M:%S")
            if now == set_alarm:
                toast.show_toast('Alarm', 'For Drinking', duration=1)
                playsound('alarm.mp3')
                break


def clear():
    hour.set(" ")
    minute.set(" ")
    second.set(" ")


root = tk.ThemedTk()
root.get_themes()
root.set_theme('blue')
root.geometry('300x250')
root.wm_maxsize(width=300, height=250)
root.wm_minsize(width=300, height=250)
root.title('Alarm Clock')
root.wm_iconbitmap('icon.ico')
root.configure(bg='darkblue')

ttk.Label(root, text='Please Set the Alarm in 24 Format', font='Helvetica 12 bold').pack(pady=5)
ttk.Label(root, text='Hours', font='Helvetica 10 bold').place(x=40, y=70)
ttk.Label(root, text='Minute', font='Helvetica 10 bold').place(x=120, y=70)
ttk.Label(root, text='Second', font='Helvetica 10 bold').place(x=200, y=70)

hour = StringVar()
minute = StringVar()
second = StringVar()

E1 = ttk.Entry(root, textvariable=hour, width=5)
E1.place(x=42, y=100)
E1.focus()

E2 = ttk.Entry(root, textvariable=minute, width=5)
E2.place(x=124, y=100)
E2.focus()

E3 = ttk.Entry(root, textvariable=second, width=5)
E3.place(x=207, y=100)
E3.focus()

B1 = ttk.Button(root, text="Set Alarm", width=10, command=alarm)
B1.place(x=30, y=150)

B2 = ttk.Button(root, text="Clear", width=10, command=clear)
B2.place(x=160, y=150)

root.mainloop()
