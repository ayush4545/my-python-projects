from tkinter import *
from tkinter.ttk import Separator, Combobox
from textblob import TextBlob


def trans():
    s1 = text1.get()
    s2 = text2.get()

    state1 = statement1.get('1.0', END)
    state2 = statement2.get('1.0', END)

    statement2.delete('1.0', END)
    word = TextBlob(str(state1))
    lan = word.detect_language()
    lang_to = lan_dict[s2]
    t = word.translate(from_lang=lan, to=lang_to)
    statement2.insert(END, t)


root = Tk()
root.geometry('500x400')
root.title('Text Translator')
root.resizable(0, 0)
root.wm_iconbitmap('icon.ico')

ss = 'Text Translator By Ayush Mishra'
l = Label(root, text='', font='poppin 15 bold', bg='#403a3a', justify=CENTER, fg='white')
l.pack(side=TOP, fill=X)

s = ''
count = 0
lan_dict = {'afrikaans': 'af', 'albanian': 'sq', 'amharic': 'am', 'arabic': 'ar', 'armenian': 'hy', 'azerbaijani': 'az',
            'basque': 'eu', 'belarusian': 'be', 'bengali': 'bn', 'bosnian': 'bs', 'bulgarian': 'bg', 'catalan': 'ca',
            'cebuano': 'ceb', 'chichewa': 'ny', 'chinese (simplified)': 'zh-cn', 'chinese (traditional)': 'zh-tw',
            'corsican': 'co', 'croatian': 'hr', 'czech': 'cs', 'danish': 'da', 'dutch': 'nl', 'english': 'en',
            'esperanto': 'eo', 'estonian': 'et', 'filipino': 'tl', 'finnish': 'fi', 'french': 'fr', 'frisian': 'fy',
            'galician': 'gl', 'georgian': 'ka', 'german': 'de', 'greek': 'el', 'gujarati': 'gu', 'haitian creole': 'ht',
            'hausa': 'ha', 'hawaiian': 'haw', 'hebrew': 'he', 'hindi': 'hi', 'hmong': 'hmn', 'hungarian': 'hu',
            'icelandic': 'is', 'igbo': 'ig', 'indonesian': 'id', 'irish': 'ga', 'italian': 'it', 'japanese': 'ja',
            'javanese': 'jw', 'kannada': 'kn', 'kazakh': 'kk', 'khmer': 'km', 'korean': 'ko',
            'kurdish (kurmanji)': 'ku', 'kyrgyz': 'ky', 'lao': 'lo', 'latin': 'la', 'latvian': 'lv', 'lithuanian': 'lt',
            'luxembourgish': 'lb', 'macedonian': 'mk', 'malagasy': 'mg', 'malay': 'ms', 'malayalam': 'ml',
            'maltese': 'mt', 'maori': 'mi', 'marathi': 'mr', 'mongolian': 'mn', 'myanmar (burmese)': 'my',
            'nepali': 'ne', 'norwegian': 'no', 'odia': 'or', 'pashto': 'ps', 'persian': 'fa', 'polish': 'pl',
            'portuguese': 'pt', 'punjabi': 'pa', 'romanian': 'ro', 'russian': 'ru', 'samoan': 'sm',
            'scots gaelic': 'gd', 'serbian': 'sr', 'sesotho': 'st', 'shona': 'sn', 'sindhi': 'sd', 'sinhala': 'si',
            'slovak': 'sk', 'slovenian': 'sl', 'somali': 'so', 'spanish': 'es', 'sundanese': 'su', 'swahili': 'sw',
            'swedish': 'sv', 'tajik': 'tg', 'tamil': 'ta', 'telugu': 'te', 'thai': 'th', 'turkish': 'tr',
            'ukrainian': 'uk', 'urdu': 'ur', 'uyghur': 'ug', 'uzbek': 'uz', 'vietnamese': 'vi', 'welsh': 'cy',
            'xhosa': 'xh', 'yiddish': 'yi', 'yoruba': 'yo', 'zulu': 'zu'}


def slider():
    global s, count
    if count >= len(ss):
        count = -1
        s = ''
        l['text'] = s
    else:
        s = s + ss[count]
        l['text'] = s
    count += 1

    l.after(200, slider)


slider()

sep = Separator(root, orient=VERTICAL)
sep.place(x=250, y=30, relwidth=0.5, relheight=0.77)

sep1 = Separator(root, orient=HORIZONTAL)
sep1.place(x=0, y=339, relwidth=1, relheight=0.75)

option = [1, 2, 3]

text1 = StringVar(root)
text1.set("Language")
text2 = StringVar(root)
text2.set("Language")

f1 = Combobox(root, textvariable=text1, state='readonly')
f1.config(width=20)
f1['values'] = [e for e in lan_dict.keys()]
f1.current(21)
f1.place(x=40, y=50)

t1 = Combobox(root, textvariable=text2, state='readonly')
t1.config(width=20)
t1['values'] = [e for e in lan_dict.keys()]
t1.current(37)
t1.place(x=300, y=50)

frame1 = Frame(root, width=200, height=225, bg='grey', relief=GROOVE, bd=3)
frame1.place(x=5, y=108)

scrollbar = Scrollbar(frame1, orient=VERTICAL)
scrollbar.grid(row=0, column=1, rowspan=5, sticky='ns')

statement1 = Text(frame1, font='poppins 13 bold', width=23, height=11, relief=GROOVE, bd=3,
                  yscrollcommand=scrollbar.set)
scrollbar.config(command=statement1.yview)
statement1.grid(row=0, column=0)

frame2 = Frame(root, width=200, height=225, bg='grey', relief=GROOVE, bd=3)
frame2.place(x=258, y=108)

scrollbar2 = Scrollbar(frame2, orient=VERTICAL)
scrollbar2.grid(row=0, column=1, rowspan=5, sticky='ns')

statement2 = Text(frame2, font='poppins 13 bold', width=23, height=11, relief=GROOVE, bd=3,
                  yscrollcommand=scrollbar2.set)
scrollbar2.config(command=statement2.yview)
statement2.grid(row=0, column=0)

b = Button(root, text='Translate Text', font='poppins 12 bold', relief=GROOVE, bd=2, command=trans)
b.place(x=195, y=350)

root.mainloop()
