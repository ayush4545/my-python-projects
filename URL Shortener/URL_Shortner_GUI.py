import pyshorteners
from tkinter import *
from ttkthemes import themed_tk as tk
from tkinter import ttk


def url_short():
    link = var.get()

    shortener = pyshorteners.Shortener()

    short_url = shortener.tinyurl.short(link)

    var1.set(short_url)


root = tk.ThemedTk()
root.get_themes()
root.set_theme('radiance')
root.geometry('350x300')
root.wm_maxsize(width=350, height=300)
root.wm_minsize(width=350, height=300)
root.title('URL Shortener')
root.wm_iconbitmap('icon.ico')

ttk.Label(root, text='Welcome To URL Shortener', font="Helvetica 15 bold italic").pack()

ttk.Label(root, text="Enter the Original URL", font="Helvetica 14 bold").place(x=65, y=50)

var = StringVar()

e1 = ttk.Entry(root, textvariable=var, width=40)
e1.place(x=45, y=90)
e1.focus()

ttk.Label(root, text="Short URL", font="Helvetica 14 bold").place(x=115, y=120)

var1 = StringVar()

ttk.Entry(root, textvariable=var1, width=40).place(x=45, y=150)

ttk.Button(root, text='Generate Short URL', width=25, command=url_short).place(x=50, y=200)

root.mainloop()
