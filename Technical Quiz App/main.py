from tkinter import *
import tkinter.messagebox as tmsg
import random
import json


def select_domain():
    global questions, answers_choice, answers
    domain = subject.get()
    print(domain)
    try:
        if domain == 'Computer Science':
            with open('cs.json', encoding="utf8") as f:
                data = json.load(f)
            questions = [v for v in data[0].values()]
            answers_choice = [v for v in data[1].values()]
            answers = [v for v in data[2].values()]
        elif domain == 'Electronics & Communication':
            with open('ec.json', encoding="utf8") as f:
                data = json.load(f)
            questions = [v for v in data[0].values()]
            answers_choice = [v for v in data[1].values()]
            answers = [v for v in data[2].values()]
        elif domain == 'General Knowledge':
            with open('gk.json', encoding="utf8") as f:
                data = json.load(f)
            questions = [v for v in data[0].values()]
            answers_choice = [v for v in data[1].values()]
            answers = [v for v in data[2].values()]

    except:
        tmsg.showerror('Error', 'Please Select Domain', icon='warning')


user_answer = []

indexes = []


def gen():
    global indexes
    while (len(indexes) < 10):
        x = random.randint(0, 9)
        if x in indexes:
            continue
        else:
            indexes.append(x)


gen()

question = 1


def btn_active(event):
    B1['state'] = ACTIVE


def arrange():
    global questions, answers_choice, c1, c2, c3, c4, btn_frame, q, submit, next, count
    global radiovar, question
    count = 30
    counting()
    x = radiovar.get()
    print(x)
    user_answer.append(x)
    radiovar.set(-1)

    if question < 10:
        q['text'] = questions[indexes[question]]
        c1['text'] = answers_choice[indexes[question]][0]
        c2['text'] = answers_choice[indexes[question]][1]
        c3['text'] = answers_choice[indexes[question]][2]
        c4['text'] = answers_choice[indexes[question]][3]
        question += 1
    if question == 10:
        submit.place(x=210, y=280)


def score():
    global user_answer, answers, indexes
    x1 = radiovar.get()
    print(x1)
    user_answer.append(x1)
    radiovar.set(-1)
    x = 0
    score = 0
    for i in indexes:
        if user_answer[x] == answers[i]:
            score += 5
        x += 1
    print(score)
    tmsg.showinfo('Score', 'Score: ' + str(score))


def counting():
    global l4, count
    l4['text'] = 'Time:' + str(count) + ' sec'
    count -= 1
    if count == 0:
        count = 30
        arrange()
    l4.after(1000, counting)


def quiz():
    l1.destroy()
    l2.destroy()
    E1.destroy()
    B1.destroy()
    f1.destroy()
    rule_instruction_label.destroy()
    sub = subject.get()
    print(sub)

    select_domain()
    n = Label(root, text='Welcome ' + name.get(), font=("poppin", 20, "bold"), bg='grey', justify=CENTER, relief=GROOVE)
    n.pack(fill=X)
    l3 = Label(root, text=sub, font=("poppin", 20, "bold"), bg='orange', justify=CENTER, relief=GROOVE)
    l3.pack(fill=X)
    global l4, count
    count = 30
    l4 = Label(root, text='Time:' + str(count) + ' sec', font=("poppin", 15, "bold"), bg='grey', justify=CENTER,
               relief=GROOVE)
    l4.pack(fill=X)

    counting()
    global questions, answers_choice, c1, c2, c3, c4, btn_frame, q, radiovar, submit, next
    q = Label(root, text=questions[indexes[0]], justify=CENTER, font=("verdana", 10, 'bold'), wraplength=400,
              relief=GROOVE, bg='lightblue', bd=3)
    q.pack(fill=X, pady=10)
    btn_frame = LabelFrame(root, text='Options', fg='white', bg='#2e2e47', font=("poppin", 15, "bold"), relief=GROOVE,
                           bd=3,
                           width=600, height=420)
    radiovar = IntVar()
    radiovar.set(-1)
    c1 = Radiobutton(btn_frame, text=answers_choice[indexes[0]][0], font=("verdana", 15, 'bold'), value=0,
                     variable=radiovar, bg='#2e2e47', fg='white', wraplength=170, activebackground='#2e2e47',
                     activeforeground='white')
    c2 = Radiobutton(btn_frame, text=answers_choice[indexes[0]][1], font=("verdana", 15, 'bold'), value=1,
                     variable=radiovar, bg='#2e2e47', fg='white', wraplength=170, activebackground='#2e2e47',
                     activeforeground='white')
    c3 = Radiobutton(btn_frame, text=answers_choice[indexes[0]][2], font=("verdana", 15, 'bold'), value=2,
                     variable=radiovar, bg='#2e2e47', fg='white', wraplength=170, activebackground='#2e2e47',
                     activeforeground='white')
    c4 = Radiobutton(btn_frame, text=answers_choice[indexes[0]][3], font=("verdana", 15, 'bold'), value=3,
                     variable=radiovar, bg='#2e2e47', fg='white', wraplength=170, activebackground='#2e2e47',
                     activeforeground='white')
    btn_frame.place(x=0, y=180)
    c1.place(x=110, y=60)
    c2.place(x=320, y=60)
    c3.place(x=110, y=160)
    c4.place(x=320, y=160)

    next = Button(btn_frame, text='Next', width=10, font=("verdana", 15, 'bold'), relief=GROOVE, bd=2, bg="#38222f",
                  fg='white', activebackground='#38222f', activeforeground='white', command=arrange)
    next.place(x=210, y=280)

    submit = Button(btn_frame, text='Submit', width=10, font=("verdana", 15, 'bold'), relief=GROOVE, bd=2, bg="#38222f",
                    fg='white', activebackground='#38222f', activeforeground='white', command=score)
    submit.place_forget()


root = Tk()
root.geometry('600x600')
root.title('Quiz App')
root.resizable(0, 0)
root.wm_iconbitmap('icon.ico')
root.configure(bg='lightgrey')
main_pic = PhotoImage(file='1st.png')
l1 = Label(root, image=main_pic, bg='lightgrey')
l1.place(x=200, y=50)

l2 = Label(root, text='Lets Some Fun to Test Your Knowledge', font=("poppin", 10, "bold"))
l2.place(x=170, y=210)

name = StringVar()
E1 = Entry(root, textvariable=name, width=33, bg='lightgrey', relief=GROOVE, bd=2)
E1.insert(0, 'Name')
E1.place(x=200, y=250)
E1.focus()

domain_choice = ['Computer Science', 'Electronics & Communication', 'General Knowledge']

subject = StringVar()
subject.set('Select Domain')
f1 = OptionMenu(root, subject, *domain_choice, )
f1.config(width=27, bg='lightgrey', relief=GROOVE, bd=2)
f1.bind('<Button-1>', btn_active)
f1.place(x=198, y=280)

b1 = PhotoImage(file='start.png')
B1 = Button(root, image=b1, bg='black', width=127, height=40, relief=GROOVE, command=quiz, bd=2, state=DISABLED)
B1.place(x=230, y=320)

text = '''
    1.This Quiz Contain 10 Question.
    2.You Get the 30 sec for each Question.
    3.Once you select a radio button that will be a final choice.
    4.Click on the Submit button to Submit your answers.
    5.Don't Try to open new tab in quiz time.
    '''
rule_instruction_label = LabelFrame(root, text='Rules & Instruction', font=("poppin", 15, "bold"), bg="grey",
                                    fg="white", bd=5, relief=GROOVE, width=600, height=200)
rule_instruction_label.place(x=0, y=395)
instruction_label = Label(rule_instruction_label, text=text, fg='white', bg='grey', justify=LEFT,
                          font=("poppin", 15, "bold"))
instruction_label.pack(padx=3)

root.mainloop()
