from random import *
import string

small = string.ascii_lowercase
cap = string.ascii_uppercase
num = '1234567890'
special = '!@#$%^&*(){}[]|?/|'

str = []
str.extend(small)
str.extend(cap)
str.extend(num)
str.extend(special)

shuffle(str)

n = input("Enter the Length of password")
n = int(n)

pwd = "".join(sample(str, n))

print(pwd)
