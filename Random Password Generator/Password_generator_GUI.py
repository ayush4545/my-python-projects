import random
from tkinter import *
from ttkthemes import themed_tk as tk
from tkinter import ttk
import string


def generate():
    length = var.get()

    small = string.ascii_lowercase
    cap = string.ascii_uppercase
    num = '1234567890'
    special = '!@#$%^&*(){}[]|?/|'

    str = []
    str.extend(small)
    str.extend(cap)
    str.extend(num)
    str.extend(special)

    random.shuffle(str)

    pwd = "".join(random.sample(str, length))

    var1.set(pwd)


root = tk.ThemedTk()
root.get_themes()
root.set_theme('radiance')
root.geometry('300x300')
root.wm_maxsize(width=300, height=300)
root.wm_minsize(width=300, height=300)
root.title('Password Generator')
root.wm_iconbitmap('icon.ico')
ttk.Label(root, text='Welcome to Password Generator', font="Helvetica 10 bold italic").pack()

l1 = ttk.Label(root, text='Enter the Length of password', font="Helvetica 10 bold italic")
l1.place(x=50, y=50)

var = IntVar()

E1 = ttk.Entry(root, textvariable=var, width=28)
E1.place(x=50, y=80)
E1.focus()

l1 = ttk.Label(root, text='Your Generated Password', font="Helvetica 10 bold italic")
l1.place(x=50, y=125)

var1 = StringVar()

E1 = ttk.Entry(root, textvariable=var1, width=28)
E1.place(x=50, y=160)

b = ttk.Button(root, text='Generate Password', width=17, command=generate)
b.place(x=50, y=200)

root.mainloop()
