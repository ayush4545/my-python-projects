from tkinter import *
import smtplib as s
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from tkinter import filedialog
from ttkthemes import themed_tk as tk
from tkinter import ttk
import os
import tkinter.messagebox as tmsg

file = ""


def Choose_file():
    global file
    file = file + filedialog.askopenfilename()
    name = os.path.split(file)
    l1['text'] = name[1]
    l1.place(x=310, y=420)


def send_email():
    global file
    name = username.get()
    pwd = password.get()
    to = To.get()
    bc = []
    bc.append(to)
    bc1 = bcc.get()
    bc.append(bc1.split(';'))

    sub = subject.get()
    msg = E6.get("1.0", "end-1c")

    try:
        ob = s.SMTP("smtp.gmail.com", 587)

        ob.starttls()

        ob.login(name, pwd)

        message = MIMEMultipart()
        message['From'] = name
        message['Subject'] = sub
        message.attach(MIMEText(msg, 'plain'))

        if len(file) > 1:
            att = open(file, 'rb')

            part = MIMEBase('application', 'octet-stream')
            part.set_payload(att.read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', "attachment; filename= " + file)
            message.attach(part)

        text = message.as_string()

        ob.sendmail(name, bc, text)
        tmsg.showinfo('Status', 'Email Send Successful')
        ob.quit()

    except Exception as e:
        print(e)
        tmsg.showinfo('Status', 'Some Connection Error \nPlease Check internet Connection')


def Clear():
    global file
    file = ""
    username.set(" ")
    password.set(" ")
    To.set(" ")
    bcc.set(" ")
    subject.set(" ")
    E6.delete("1.0", "end-1c")
    l1.place_forget()


root = tk.ThemedTk()
root.get_themes()
root.set_theme('radiance')
root.geometry('500x530')
root.wm_maxsize(width=500, height=530)
root.wm_minsize(width=500, height=530)
root.title('Email Sender')
root.wm_iconbitmap('icon.ico')

ttk.Label(root, text='Welcome To Smart Email Sender', font="Helvetica 15 bold italic").pack()
l = ['Username', 'Password', 'To', 'bcc', 'Subject', 'body', 'Attachment']
x = 30
y = 50

for item in l:
    if item == 'body':
        exec('ttk.Label(root, text="{}", font="Helvetica 15 bold").place(x={},y={})'.format(item, x, y))
        y = y + 160
    else:
        exec('ttk.Label(root, text="{}", font="Helvetica 15 bold").place(x={},y={})'.format(item, x, y))
        y = y + 40

username = StringVar()
E1 = ttk.Entry(root, textvariable=username, width=46)
E1.place(x=170, y=55)
E1.focus()

password = StringVar()
E2 = ttk.Entry(root, textvariable=password, width=46,show="*")
E2.place(x=170, y=95)

To = StringVar()
E3 = ttk.Entry(root, textvariable=To, width=46)
E3.place(x=170, y=135)

bcc = StringVar()
E4 = ttk.Entry(root, textvariable=bcc, width=46)
E4.place(x=170, y=175)

subject = StringVar()
E5 = ttk.Entry(root, textvariable=subject, width=46)
E5.place(x=170, y=215)

E6 = Text(root, width=35, height=8, relief=SOLID)
E6.place(x=170, y=255)

B1 = ttk.Button(root, text="Choose File", command=Choose_file)
B1.place(x=170, y=410)

l1 = ttk.Label(root, text="file")
l1.place_forget()

B2 = ttk.Button(root, text="Send", command=send_email)
B2.place(x=80, y=460)

B3 = ttk.Button(root, text="Clear", command=Clear)
B3.place(x=280, y=460)

root.mainloop()
