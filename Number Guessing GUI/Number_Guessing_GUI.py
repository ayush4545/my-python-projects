import random
from tkinter import *


def check():
    global number, l1, initial, final
    guess = var3.get()



    if guess == number:
        msg = "You Win"

    elif guess > number:
        msg = 'Higher'

    elif guess < number:
        msg = "Lower"

    l1['text'] = msg


root = Tk()
root.geometry('300x300')
root.title('Number Guessing')
root.configure(bg="silver")

var1 = IntVar()
var2 = IntVar()
var3 = IntVar()

Label(root, text="From", font="Helvetica 16 bold italic", fg="Blue",bg="silver").place(x=10, y=10)
Entry(root, textvariable=var1, width=35).place(x=80, y=15)

Label(root, text="To", font="Helvetica 16 bold italic", fg="Blue",bg="silver").place(x=10, y=45)
Entry(root, textvariable=var2, width=35).place(x=80, y=50)

Label(root, text="Enter the Guessing Number", font="Times 16 bold italic", fg="Blue",bg="silver").place(x=25, y=100)
Entry(root, textvariable=var3, width=40).place(x=30, y=150)

initial = var1.get()
final = var2.get()
number = random.randint(initial, final)
Button(root, text="Check", width=20, bg='purple', fg="white", command=check).place(x=75, y=185)
l1 = Label(root, text="Result", font="Helvetica 16 bold italic", fg="Green",bg="silver")
l1.pack(side=BOTTOM, pady=50)

root.mainloop()
