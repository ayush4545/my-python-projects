from ttkthemes import themed_tk as tk
from tkinter import ttk
import requests
import json
import tkinter.messagebox as tmsg
from tkinter import *

def send_sms():
    number=n.get()
    message=t1.get("1.0", "end-1c")
    try:
        url='https://www.fast2sms.com/dev/bulk'
        params={
            'authorization':'d3tGCKcP4gwRxyErMIfloWNJaOmX9ehSBA8si0ZHpn61DuFVLjA04HSLKOJ8WVcFpD9Ngd5ITzjBrMYl',
            'sender_id':'FSTSMS',
            'message':message,
            'language':'english',
            'route':'p',
            'numbers':number,
        }
        response=requests.get(url,params=params)
        dic=response.json()
        print(dic)
        tmsg.showinfo('Status','Message Send Successfully!')
    except Exception as e:
        print(e)
        tmsg.showinfo('Status', 'Message Not send Due to Some Problem')
root = tk.ThemedTk()
root.get_themes()
root.set_theme('radiance')
root.geometry('400x530')
root.wm_maxsize(width=400, height=530)
root.wm_minsize(width=400, height=530)
root.title('SMS Sender')
root.wm_iconbitmap('icon.ico')

ttk.Label(root,text='Welcome to SMS Sender',font="Helvetica 15 bold italic").pack()


ttk.Label(root,text='Enter Mobile Number',font="Helvetica 15 bold").place(x=100,y=50)


n=StringVar()
e1=ttk.Entry(root,textvariable=n,width=58)
e1.place(x=20,y=80)
e1.focus()

ttk.Label(root,text='Message',font="Helvetica 15 bold").place(x=150,y=120)

t1=Text(root,width=50,height=15,font="Helvetica 10 bold",relief=SOLID)
t1.place(x=20,y=150)

b1=ttk.Button(root,text='Send',width=15,command=send_sms)
b1.place(x=130,y=430)


root.mainloop()