from tkinter import *
from forex_python.converter import CurrencyRates
from tkinter import ttk
from ttkthemes import themed_tk as tk
import tkinter.messagebox as tmsg


def con():
    try:
        a = f.get()
        b = t.get()
        price = amount.get()
        amt = c.convert(a, b, price)
        ex_amount.set(amt)

    except:
        tmsg.showerror('Error', 'Please enter only Numerical Valid Value')


root = tk.ThemedTk()
root.get_themes()
root.set_theme('radiance')
root.geometry('400x320')
root.wm_maxsize(width=400, height=320)
root.wm_minsize(width=400, height=320)
root.wm_title('Currency Converter')
root.wm_iconbitmap('cv.ico')

ttk.Label(root, text="From", font="Helvetica 16 bold italic").place(x=30, y=30)
ttk.Label(root, text="To", font="Helvetica 16 bold italic").place(x=30, y=65)
ttk.Label(root, text="Amount", font="Helvetica 16 bold italic").place(x=30, y=95)
ttk.Label(root, text="Exchange\nAmount", font="Helvetica 16 bold italic").place(x=30, y=150)

option = ['INR', 'USD', 'EUR', 'GBP', 'AUD', 'CAD', 'BGN', 'ILS', 'IDR', 'DKK', 'JPY', 'HUF', 'INR']
c = CurrencyRates()

f = StringVar(root)
f.set("Currency1")
t = StringVar(root)
t.set("Currency2")

f1 = ttk.OptionMenu(root, f, *option)
f1.config(width=15)
f1.place(x=150, y=30)

t1 = ttk.OptionMenu(root, t, *option)
t1.config(width=15)
t1.place(x=150, y=65)

amount = DoubleVar()
E1 = ttk.Entry(root, textvariable=amount, width=22)
E1.place(x=150, y=100)
E1.focus()

ex_amount = DoubleVar()
E2 = ttk.Entry(root, textvariable=ex_amount, width=22)
E2.place(x=150, y=160)
E2.focus()

b1 = ttk.Button(root, text='Convert', command=con, width=13)
b1.place(x=150, y=210)

root.mainloop()
