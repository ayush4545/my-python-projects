from tkinter import *
import random
import tkinter.messagebox as tmsg


def startgame(event):
    global score, mismatched
    if time_left == 60:
        time()
    l5['text'] = ''
    if (e1.get() == l4['text']):
        score += 1
        l2['text'] = 'Score:' + str(score)
    else:
        mismatched += 1
    random.shuffle(words)
    l4.configure(text=words[0])
    e1.delete(0, END)


def time():
    global time_left, score, mismatched
    if time_left >= 11:
        pass
    else:
        l3.configure(fg='red')
    if time_left > 0:
        time_left -= 1
        l3['text'] = 'Time Left: ' + str(time_left)
        l3.after(1000, time)

    else:
        l5.configure(text='Match:{} | Missmatch:{} | Total Score:{}'.format(score, mismatched, abs(score - mismatched)))
        t = tmsg.askretrycancel('Notification', 'If you want to play again Hit Retry')
        if t == True:
            score = 0
            time_left = 60
            mismatched = 0
            l2['text'] = 'Score:' + str(score)
            l3['text'] = 'Time Left: ' + str(time_left)
            random.shuffle(words)
            l4.configure(text=words[0])
            l5['text'] = 'Type the Given word and press Enter'


root = Tk()
root.geometry('800x600+400+100')
root.resizable(0, 0)
root.title('Typing Speed Game')
root.wm_iconbitmap('icon.ico')
root.configure(bg='#333359')
# =================================================

ss = "Typing Speed Game Developed By Ayush Mishra"
l1 = Label(root, font=("Comicsansms", 20, "bold"), text='', bg='grey', fg='white', justify=CENTER)
l1.pack(fill=X, side=TOP, ipady=5)
s = ''
count = 0
score = 0
mismatched = 0


def slider():
    global s, count
    if count >= len(ss):
        count = -1
        s = ''
        l1['text'] = s
    else:
        s = s + ss[count]
        l1['text'] = s
    count += 1

    l1.after(200, slider)


slider()

l2 = Label(root, text='Score: ' + str(score), font=("Comicsansms", 20, "bold"), fg='white', bg='#333359')
l2.place(x=20, y=80)

time_left = 60

l3 = Label(root, text='Time Left: ' + str(time_left), font=("Comicsansms", 20, "bold"), fg='white', bg='#333359')
l3.place(x=600, y=80)

words = ['Monkey', 'Keyboard', 'Man', 'purple', 'blue', 'understand', 'apology', 'ayush']

random.shuffle(words)

l4 = Label(root, text=words[0].capitalize(), font=("Comicsansms", 20, "bold"), fg='white', bg='#333359')
l4.place(x=350, y=200)

type = StringVar()
e1 = Entry(root, textvariable=type, width=40, font=("Comicsansms", 15, "bold"), justify=CENTER)
e1.focus()
e1.place(x=200, y=270)

l5 = Label(root, text='Type the Given word and press Enter', font=("Comicsansms", 25, "bold"), fg='white', bg='#333359')
l5.place(x=120, y=400)

e1.bind('<Return>', startgame)

root.mainloop()
