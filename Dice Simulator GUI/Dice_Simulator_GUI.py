from tkinter import *
import tkinter.messagebox as tmsg
from random import *

def roll():#function for roll button
    a = int(var.get())
    number = randint(1, 6)

    while True:
        if number == 1:
            Label(root, image=p[0]).place(x=280, y=100)

        if number == 2:
            Label(root, image=p[1]).place(x=280, y=100)

        if number == 3:
            Label(root, image=p[2]).place(x=280, y=100)

        if number == 4:
            Label(root, image=p[3]).place(x=280, y=100)

        if number == 5:
            Label(root, image=p[4]).place(x=280, y=100)

        if number == 6:
            Label(root, image=p[5]).place(x=280, y=100)

        if a == number:
            tmsg.showinfo('Result', 'You Win')

        break


root = Tk()
root.geometry('400x250')
root.wm_maxsize(width=400, height=250)
root.title("Dice Simulator")
root.wm_iconbitmap("image/icon.ico")

# left frame - user input ,live etc
# right frame- dice

left_frame = Frame(root, bg='blue', width=250, height=250)
left_frame.pack(side=LEFT)

right_frame = Frame(root, width=150, height=250)
right_frame.pack(side=RIGHT)

l1 = Label(left_frame, text="Pressed Number Between 1 to 6", bg="blue", fg="white")
l1.place(x=20, y=50)

var = StringVar()
var.set(" ")
input_var = Entry(left_frame, textvariable=var)
input_var.place(x=35, y=75)

b1 = Button(left_frame, text="Roll", width=15, command=roll)
b1.place(x=40, y=110)

p = [PhotoImage(file='1.png'), PhotoImage(file='image/2.png'), PhotoImage(file='image/3.png'),
     PhotoImage(file='image/4.png'), PhotoImage(file='image/5.png'), PhotoImage(file='image/5.png')]

root.mainloop()
