from tkinter import *
import tkinter.messagebox as tmsg
import random

live = 6
count = 0
score = 0
guessword = ""


def check(button, letter):
    global count, w, score, word, guessword, main, l2, live
    guessword = guessword + letter
    str1 = ""
    for i, k in enumerate(word):
        if k in guessword:
            l = list(main)
            l[i] = k
            main = str1.join(l)

    if letter not in word:
        count += 1
        live -= 1
        if count == 1:
            h['image'] = hang[1]
        if count == 2:
            h['image'] = hang[2]
        if count == 3:
            h['image'] = hang[3]
        if count == 4:
            h['image'] = hang[4]
        if count == 5:
            h['image'] = hang[5]
        if count == 6:
            h['image'] = hang[6]
            answer = tmsg.askyesno('GAME OVER', 'YOU LOST!\nWANT TO PLAY AGAIN?')
            if answer == True:
                word = random.choice(
                    ['abruptly', 'absurd', 'abyss', 'affix', 'buffalo', 'azure', 'banjo', 'bikini', 'car', 'men', 'man',
                     'blue',
                     'white', 'green', 'black', 'buzzwords', 'cycle', 'galaxy', 'joyful', 'able', 'about', 'account',
                     'acid', 'across', 'act', 'addition', 'advertisement', 'agreement', 'against',
                     'between', 'birth'])
                main = ""
                for i in range(len(word)):
                    main = main + "*"
                l2['text'] = main
                h['image'] = hang[0]
                main_func()
            else:
                root.destroy()
    l2['text'] = main
    c['text'] = "Live: " + str(live)

    if main == word:
        score += 10
        s['text'] = "Score: " + str(score)
        print(score)
        answer = tmsg.askyesno('GAME OVER', 'YOU WON!\nWANT TO PLAY AGAIN?')
        if answer == True:
            word = random.choice(
                ['abruptly', 'absurd', 'abyss', 'affix', 'buffalo', 'azure', 'banjo', 'bikini', 'car', 'men', 'man',
                 'blue',
                 'white', 'green', 'black', 'buzzwords', 'cycle', 'galaxy', 'joyful', 'able', 'about', 'account',
                 'acid', 'across', 'act', 'addition', 'advertisement', 'agreement', 'against',
                 'between', 'birth'])
            main = ""
            for i in range(len(word)):
                main = main + "*"
            l2['text'] = main
            h['image'] = hang[0]
            live = 6
            c['text'] = "Live: " + str(live)
            main_func()
        else:
            root.destroy()


def main_func():
    f1.pack_forget()
    f2['height'] = 650
    global valid_letter, hang, alphabet, B, word, main
    name = var.get()
    text = "Welcome " + name + " in Hangman Game"
    Label(f2, text=text, font="Helvetica 16 bold italic", bg='green', fg='white').place(x=250, y=5)
    initial = 0
    final = 9
    x = 20
    y = 50

    for i in range(3):
        if i != 2:
            for j in range(initial, final):
                exec("{}=Button(f2, image=alphabet[j],bg='green',command=lambda:check('{}','{}'))".format(B[j][0],
                                                                                                          B[j][0],
                                                                                                          B[j][1]))
                exec('{}.place(x={},y={})'.format(B[j][0], x, y))
                x = x + 85
            initial = initial + final
            final = final * 2
            x = 20
            y = y + 60
        elif i == 2:
            for j in range(18, 26):
                exec("{}=Button(f2, image=alphabet[j],bg='green',command=lambda:check('{}','{}'))".format(B[j][0],
                                                                                                          B[j][0],
                                                                                                          B[j][1]))
                exec('{}.place(x={},y={})'.format(B[j][0], x, y))
                x = x + 85

        h.place(x=30, y=250)

        s.place(x=10, y=5)
        l2.place(x=390, y=400)
        c.place(x=700, y=5)
        exit.place(x=10, y=590)


def close():
    answer = tmsg.askyesno('ALERT', 'YOU WANT TO EXIT THE GAME?')
    if answer == True:
        root.destroy()


# Choose Random word
word = random.choice(
    ['abruptly', 'absurd', 'abyss', 'affix', 'buffalo', 'azure', 'banjo', 'bikini', 'car', 'men', 'man', 'blue',
     'white', 'green', 'black', 'buzzwords', 'cycle', 'galaxy', 'joyful', 'able', 'about', 'account', 'acid', 'across',
     'act', 'addition', 'advertisement', 'agreement', 'against',
     'between', 'birth'])

root = Tk()
root.geometry('800x650')
root.wm_maxsize(width=800, height=650)
root.wm_minsize(width=800, height=650)
root.title('Hangman')
root.wm_iconbitmap('icon.ico')

f1 = Frame(root, height=200, bg="lightblue", width=400)
L1 = Label(f1, text='Enter Your Name', bg="lightblue", font="Helvetica 16 bold italic", fg='purple')
L1.pack(pady=10)
var = StringVar()

E1 = Entry(f1, textvariable=var, width=30, bg='silver')
E1.pack(pady=5)
E1.focus()

b1 = Button(f1, text='Submit', command=main_func, width=15, bg='blue', fg='white')
b1.pack(pady=10)

f1.pack(fill=X)
f2 = Frame(root, height=550, bg="green", width=400)
f2.pack(fill=BOTH)

# list of photo of all alphabet
alphabet = [PhotoImage(file='img/HANGMAN-GAME-master/a.png'), PhotoImage(file='img/HANGMAN-GAME-master/b.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/c.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/d.png'), PhotoImage(file='img/HANGMAN-GAME-master/e.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/f.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/g.png'), PhotoImage(file='img/HANGMAN-GAME-master/h.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/i.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/j.png'), PhotoImage(file='img/HANGMAN-GAME-master/k.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/l.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/m.png'), PhotoImage(file='img/HANGMAN-GAME-master/n.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/o.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/p.png'), PhotoImage(file='img/HANGMAN-GAME-master/q.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/r.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/s.png'), PhotoImage(file='img/HANGMAN-GAME-master/t.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/u.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/v.png'), PhotoImage(file='img/HANGMAN-GAME-master/w.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/x.png'),
            PhotoImage(file='img/HANGMAN-GAME-master/y.png'), PhotoImage(file='img/HANGMAN-GAME-master/z.png')]

# list of all hangman photo
hang = [PhotoImage(file='img/HANGMAN-GAME-master/h1.png'), PhotoImage(file='img/HANGMAN-GAME-master/h2.png'),
        PhotoImage(file='img/HANGMAN-GAME-master/h3.png'),
        PhotoImage(file='img/HANGMAN-GAME-master/h4.png'), PhotoImage(file='img/HANGMAN-GAME-master/h5.png'),
        PhotoImage(file='img/HANGMAN-GAME-master/h6.png'), PhotoImage(file='img/HANGMAN-GAME-master/h7.png')]

# list of all button along their alphabet
B = [['b1', 'a'], ['b2', 'b'], ['b3', 'c'], ['b4', 'd'], ['b5', 'e'], ['b6', 'f'], ['b7', 'g'], ['b8', 'h'],
     ['b9', 'i'], ['b10', 'j'], ['b11', 'k'], ['b12', 'l'], ['b13', 'm'], ['b14', 'n'], ['b15', 'o'], ['b16', 'p'],
     ['b17', 'q'], ['b18', 'r'], ['b19', 's'], ['b20', 't'], ['b21', 'u'], ['b22', 'v'], ['b23', 'w'], ['b24', 'x'],
     ['b25', 'y'], ['b26', 'z']]

# main is used to create * pattern of length of choose word
main = ""

for i in range(len(word)):
    main = main + "*"

l2 = Label(f2, text=main, bg="green", font=("arial", 50))
l2.place(x=350, y=300)
l2.place_forget()

s = Label(f2, text="Score: " + str(score), fg="yellow", font="Helvetica 16 bold italic", bg='green')
s.place_forget()
c = Label(f2, text="Live: " + str(live), fg="yellow", font="Helvetica 16 bold italic", bg='green')
c.place_forget()

h = Label(f2, image=hang[0], bg='green')
h.place_forget()

e = PhotoImage(file='img/HANGMAN-GAME-master/exit.png')
exit = Button(f2, image=e, bg="green", command=close)
exit.place_forget()
root.mainloop()
