import random

print("welcome to Hangman game")
turns = 10
word = random.choice(
    ['abruptly', 'absurd', 'abyss', 'affix', 'buffalo', 'azure', 'banjo', 'bikini', 'car', 'men', 'man', 'blue',
     'white', 'green', 'black', 'buzzwords', 'cycle', 'galaxy', 'joyful'])
valid_letter = 'abcdefghijklmnopqrstuvwxyz'
guessword = ""

while len(word) > 0:
    main = ""
    for letter in word:
        if letter in guessword:
            main = main + letter
        else:
            main = main + "_" + ""

    if main == word:
        print(main)
        print('You win')
        break
    print('Guess the word:', main)
    guess = input()
    guess = guess.lower()

    if guess in valid_letter:
        guessword = guessword + guess
    else:
        print('enter the valid character')
        guess = input()
        guess = guess.lower()

    if guess not in word:
        turns -= 1
        if turns == 9:
            print("9 turns are left")
            print(" --------- ")
        if turns == 8:
            print("8 turns are left")
            print(" --------- ")
            print("     o     ")
        if turns == 7:
            print("7 turns are left")
            print(" --------- ")
            print("     o     ")
            print("     |     ")
        if turns == 6:
            print("6 turns are left")
            print(" --------- ")
            print("     0     ")
            print("     |     ")
            print("   /       ")
        if turns == 5:
            print("5 turns are left")
            print(" --------- ")
            print("     0     ")
            print("     |     ")
            print("   /   \   ")
        if turns == 4:
            print("4 turns are left")
            print(" --------- ")
            print("   \ 0     ")
            print("     |     ")
            print("   /   \   ")
        if turns == 3:
            print("3 turns are left")
            print(" --------- ")
            print("   \ 0 /   ")
            print("     |     ")
            print("   /   \   ")
        if turns == 2:
            print("2 turns are left")
            print(" --------- ")
            print("   \ 0 /|  ")
            print("     |     ")
            print("   /   \   ")
        if turns == 1:
            print("1 turns are left")
            print("Last breath counting,take care!")
            print(" --------- ")
            print("   \ 0_|/  ")
            print("     |     ")
            print("   /   \   ")
        if turns == 0:
            print("YOU LOSE")
            print("You let a kind man die")
            print(" --------- ")
            print("     0_|   ")
            print("    /|\    ")
            print("    / \    ")
