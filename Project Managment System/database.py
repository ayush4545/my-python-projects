import sqlite3

def connect():
    conn = sqlite3.connect('PROJECT.db')
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS Projects (Id INTEGER PRIMARY KEY AUTOINCREMENT,Project_name text, LANGUAGE TEXT, START_DATE text , END_DATE TEXT, DESCRIPTION TEXT)")
    conn.commit()
    conn.close()

def insert(name,language,start_Date,end_Date,desc):
    conn = sqlite3.connect('PROJECT.db')
    cur = conn.cursor()
    query = "insert into 'Projects' (Project_name,language,start_date,end_date,description) values(?,?,?,?,?)"
    cur.execute(query, (name,language,start_Date,end_Date,desc))
    conn.commit()
    conn.close()

def display():
    conn = sqlite3.connect('PROJECT.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM 'Projects'")
    rows = cur.fetchall()
    conn.commit()
    conn.close()
    return rows

def delete(id):
    conn = sqlite3.connect('PROJECT.db')
    cur = conn.cursor()
    query="DELETE FROM 'projects' WHERE id=? "
    cur.execute(query, (id,))
    conn.commit()
    conn.close()

def search(id):
    conn = sqlite3.connect('PROJECT.db')
    cur = conn.cursor()
    query = "Select * from 'Projects' where id=?"
    cur.execute(query, (id,))
    rows = cur.fetchone()
    conn.commit()
    conn.close()
    return rows

def update(name,language,start_Date,end_Date,desc,id):
    conn = sqlite3.connect('PROJECT.db')
    cur = conn.cursor()
    query = "update 'Projects' set Project_name=?,language=?,start_date=?,end_date=?,description=? where id=?"
    cur.execute(query, (name,language,start_Date,end_Date,desc,id))
    conn.commit()
    conn.close()

connect()
