from tkinter import *
from tkinter.ttk import Separator
import tkinter.messagebox as tmsg
from database import insert,display,delete,search,update

def add():
    n=name.get()
    start_date=sdate.get()
    end_date=edate.get()
    language=lang.get()
    description=t.get('1.0',END)

    print(n,start_date,end_date,language,description)

    if n and language and start_date and end_date and description != "":
        try:
            insert(n, language, start_date, end_date, description)
            tmsg.showinfo('Success', 'Project Details added successfully')
            name.set('')
            lang.set('')
            sdate.set('')
            edate.set('')
            t.delete('1.0', END)
        except Exception as e:
            tmsg.showerror('Error', str(e), icon='warning')

    else:
        tmsg.showerror('Error', 'Please fill Required Fields', icon='warning')

def updated():
    n = name.get()
    start_date = sdate.get()
    end_date = edate.get()
    language = lang.get()
    description = t.get('1.0', END)

    selected_item = t.curselection()
    item = t.get(selected_item)
    item = item.split(" ")
    id = int(item[0])

    print(n, start_date, end_date, language, description)

    if n and language and start_date and end_date and description != "":
        try:
            update(n, language, start_date, end_date, description,id)
            tmsg.showinfo('Success', 'Project Details updated successfully')
        except Exception as e:
            tmsg.showerror('Error', str(e), icon='warning')

    else:
        tmsg.showerror('Error', 'Please fill Required Fields', icon='warning')

def displayed():
    global length
    rows=display()
    list_box.delete(1,END)
    count=1
    ss=" "
    ll=0
    for row in rows:
        if len(row[5])>40:
            ss=row[5][:30]+'.....'
        else:
            ss=row[5]
        if len(row[1])>13:
            s1=row[1][:14]
        else:
            s1=row[1]
        list_box.insert(count,str(row[0])+'            '+s1+'              '+row[2]+'                  '+row[3]+'                 '+row[4]+'       '+ss)
        count+=1
        ll=row[0]
    length=count
    L2['text']='Display Data (Total Projects:'+str(ll-3)+')'

def deleted():
    selected_item = list_box.curselection()
    item = list_box.get(selected_item)
    item = item.split(" ")
    id = int(item[0])
    delete(id)
    list_box.delete(selected_item)

def searched():
    selected_item = list_box.curselection()
    item = list_box.get(selected_item)
    item = item.split(" ")
    id = int(item[0])
    row=search(id)

    name.set(row[1])
    lang.set(row[2])
    sdate.set(row[3])
    edate.set(row[4])
    t.insert(END,row[5])

def clear():
    name.set('')
    lang.set('')
    sdate.set('')
    edate.set('')
    t.delete('1.0',END)

    global length
    list_box.delete(1,length)


root = Tk()
root.geometry('1100x600')
root.title('Personal Project Managment System')
root.wm_iconbitmap('icon.ico')
root.configure(bg='#29384a')

ss = "Project Managment System"
l1 = Label(root, font=("Comicsansms", 20, "bold"), text='', bg='grey', fg='white', justify=CENTER, relief=GROOVE)
l1.pack(fill=X, side=TOP, ipady=5)
s = ''
count = 0


def slider():
    global s, count
    if count >= len(ss):
        count = -1
        s = ''
        l1['text'] = s
    else:
        s = s + ss[count]
        l1['text'] = s
    count += 1

    l1.after(200, slider)


slider()

# ===========================================Label Section===================================
L1 = LabelFrame(root, text='Project Details', width=480, height=500, relief=GROOVE, bd=5,fg='white',bg='#29384a',
                font=("Comicsansms", 20, "bold"))
L1.place(x=30, y=70)

Label(L1, text='Project Name', font=("Comicsansms", 12, "bold"),fg='white',bg='#29384a').place(x=20, y=30)
Label(L1, text='Language', font=("Comicsansms", 12, "bold"),fg='white',bg='#29384a').place(x=20, y=80)
Label(L1, text='Start Date', font=("Comicsansms", 12, "bold"),fg='white',bg='#29384a').place(x=20, y=130)
Label(L1, text='End Date', font=("Comicsansms", 12, "bold"),fg='white',bg='#29384a').place(x=265, y=130)
Label(L1, text='Description', font=("Comicsansms", 12, "bold"),fg='white',bg='#29384a').place(x=20, y=180)

# ==========================================Entry Section===============================
name=StringVar()
lang=StringVar()
sdate=StringVar()
edate=StringVar()
e1 = Entry(L1, width=48, relief=GROOVE, bd=2,textvariable=name)
e1.place(x=150, y=30)

e2 = Entry(L1, width=48, relief=GROOVE, bd=2,textvariable=lang)
e2.place(x=150, y=80)

e3 = Entry(L1, width=15, relief=GROOVE, bd=2,textvariable=sdate)
e3.place(x=150, y=130)

e4 = Entry(L1, width=15, relief=GROOVE, bd=2,textvariable=edate)
e4.place(x=350, y=130)

t = Text(L1, width=36, height=5, relief=GROOVE, bd=2, font=("poppins", 11))
t.place(x=150, y=180)

# ======================================Button Section=======================================
b1 = Button(L1, text='Add', width=10, relief=GROOVE, font=("poppins", 11),command=add)
b1.place(x=20, y=320)

b2 = Button(L1, text='Update', width=10, relief=GROOVE, font=("poppins", 11),command=updated)
b2.place(x=180, y=320)

b3 = Button(L1, text='Display', width=10, relief=GROOVE, font=("poppins", 11),command=displayed)
b3.place(x=340, y=320)

b4 = Button(L1, text='Delete', width=10, relief=GROOVE, font=("poppins", 11),command=deleted)
b4.place(x=20, y=380)

b6 = Button(L1, text='Search', width=10, relief=GROOVE, font=("poppins", 11),command=searched)
b6.place(x=180, y=380)

b5 = Button(L1, text='Clear', width=10, relief=GROOVE, font=("poppins", 11),command=clear)
b5.place(x=340, y=380)

# ======================================================Lableframe2 for display data===========================
L2 = LabelFrame(root, text='Display Data (Total Projects:0)', width=600, height=500, relief=GROOVE, bd=5, font=("Comicsansms", 20, "bold"),fg='white',bg='#29384a')
L2.place(x=520, y=70)

scrollbar = Scrollbar(L2, orient=VERTICAL)
scrollbar.grid(row=0, column=1, rowspan=5, sticky='ns')

scrollbar1 = Scrollbar(L2, orient=HORIZONTAL)
scrollbar1.grid(row=1, column=0, columnspan=5, sticky='we')



list_box = Listbox(L2, height=28, width=90,selectmode=SINGLE,
                    yscrollcommand=scrollbar.set,xscrollcommand=scrollbar1.set)
scrollbar.config(command=list_box.yview)
scrollbar1.config(command=list_box.xview)

list_box.insert(0,'S.NO      Project Name          Language           Start Date          Date End                      Description')
list_box.grid(row=0,column=0)
sep = Separator(list_box, orient=VERTICAL)
sep.place(x=30, y=0, relwidth=0, relheight=0.04)

sep = Separator(list_box, orient=VERTICAL)
sep.place(x=130, y=0, relwidth=0, relheight=0.04)

sep = Separator(list_box, orient=VERTICAL)
sep.place(x=220, y=0, relwidth=0, relheight=0.04)

sep = Separator(list_box, orient=VERTICAL)
sep.place(x=300, y=0, relwidth=0, relheight=0.04)

sep = Separator(list_box, orient=VERTICAL)
sep.place(x=380, y=0, relwidth=0, relheight=0.04)

sep = Separator(list_box, orient=HORIZONTAL)
sep.place(x=0, y=17, relwidth=1, relheight=0)


root.mainloop()
